# pyforms
A python 2.7 module to interface with Windows Forms.
Requires .NET Framework 4.

## Installation
Upon cloning the project, build the Visual Studio project to generate the backend executable.
Then, proceed as usual:
```
python setup.py install
```

## Usage
In your python code, simply import the module:
```
from pyforms import *
```
Examples are available in the [example.py](../master/example.py) file.