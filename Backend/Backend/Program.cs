﻿using System;
using System.Windows.Forms;

namespace Backend
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Window(args));
        }
    }
}
