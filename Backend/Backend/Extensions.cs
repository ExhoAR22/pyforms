﻿using System;
using System.Net.Sockets;
using System.Text;

namespace Backend
{
    static class Extensions
    {
        public static int SendString(this Socket socket, string s)
        {
            return socket.Send(Encoding.ASCII.GetBytes(s));
        }

        public static string ReceiveString(this Socket socket)
        {
            string res = "";
            byte[] buff = new byte[1];
            socket.Receive(buff);
            while ((char)buff[0] != '\n')
            {
                res += (char)buff[0];
                socket.Receive(buff);
            }
            return res;
        }
    }
}
