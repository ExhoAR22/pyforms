﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace Backend
{
    public partial class Window : Form
    {
        private Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp),
            worker_client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private Thread thread;
        private bool initialized = false;
        private Dictionary<string, Control> views = new Dictionary<string, Control>();
        private Dictionary<Control, string> ids = new Dictionary<Control, string>();

        public void OnButtonClick(object sender, EventArgs e)
        {
            worker_client.SendString("button_clicked " + ids[(Control)sender]);
        }

        public void OnTBTextChanged(object sender, EventArgs e)
        {
            worker_client.SendString("textbox_text_changed " + ids[(Control)sender]);
        }

        public void OnChkCheckedChanged(object sender, EventArgs e)
        {
            worker_client.SendString("checkable_checked_changed " + ids[(Control)sender]);
        }

        public void OnRadioCheckedChanged(object sender, EventArgs e)
        {
            worker_client.SendString("checkable_checked_changed " + ids[(Control)sender]);
        }

        public Window(string[] args)
        {
            InitializeComponent();
            this.Width = int.Parse(args[1]);
            this.Height = int.Parse(args[2]);
            this.Text = args[3];
            client.Connect(new IPEndPoint(IPAddress.Parse("127.0.0.1"), int.Parse(args[0])));
            worker_client.Connect(new IPEndPoint(IPAddress.Parse("127.0.0.1"), int.Parse(args[0])));
            thread = new Thread(ThreadProc);
            thread.Start();
            initialized = true;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            worker_client.SendString("window_closed");
            thread.Abort();
            client.Close();
            worker_client.Close();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (initialized)
            {
                if (WindowState == FormWindowState.Minimized)
                {
                    worker_client.SendString("window_minimized");
                }
                else if (WindowState == FormWindowState.Normal)
                {
                    worker_client.SendString("window_restored");
                }
            }
        }

        private string JoinInBounds(string[] arr, char c, int from, int to)
        {
            string s = "";
            for (int i = from; i < to; i++)
            {
                s += arr[i] + c;
            }
            s = s.Substring(0, s.Length - 1);
            return s;
        }

        public void ThreadProc()
        {
            try
            {
                string[] req = client.ReceiveString().Split(' ');
                while (req[0] != "")
                {
                    if (req[0] == "close")
                    {
                        this.Invoke((MethodInvoker)delegate { Application.Exit(); });
                    }
                    else if (req[0] == "set_text")
                    {
                        this.Invoke((MethodInvoker)delegate { this.Text = JoinInBounds(req, ' ', 1, req.Length); });
                    }
                    else if (req[0] == "get_text")
                    {
                        client.SendString("*" + this.Text);
                    }
                    else if (req[0] == "set_width")
                    {
                        this.Invoke((MethodInvoker)delegate { this.Width = int.Parse(req[1]); });
                    }
                    else if (req[0] == "get_width")
                    {
                        client.SendString(this.Width.ToString());
                    }
                    else if (req[0] == "set_height")
                    {
                        this.Invoke((MethodInvoker)delegate { this.Height = int.Parse(req[1]); });
                    }
                    else if (req[0] == "get_height")
                    {
                        client.SendString(this.Height.ToString());
                    }
                    else if (req[0] == "is_minimized")
                    {
                        client.SendString(this.WindowState == FormWindowState.Minimized ? "True" : "False");
                    }
                    else if (req[0] == "set_minimized")
                    {
                        this.Invoke((MethodInvoker)delegate {
                            this.WindowState = bool.Parse(req[1]) ? FormWindowState.Minimized : FormWindowState.Normal;
                        });
                    }
                    else if (req[0] == "add_button")
                    {
                        this.Invoke((MethodInvoker)delegate {
                            Button button = new Button();
                            views.Add(req[1], button);
                            button.Location = new System.Drawing.Point(int.Parse(req[2]), int.Parse(req[3]));
                            button.Width = int.Parse(req[4]);
                            button.Height = int.Parse(req[5]);
                            button.Text = JoinInBounds(req, ' ', 6, req.Length);
                            button.Click += new EventHandler(OnButtonClick);
                            Controls.Add(button);
                            ids.Add(button, req[1]);
                        });
                    }
                    else if (req[0] == "add_textbox")
                    {
                        this.Invoke((MethodInvoker)delegate {
                            TextBox tb = new TextBox();
                            views.Add(req[1], tb);
                            tb.Location = new System.Drawing.Point(int.Parse(req[2]), int.Parse(req[3]));
                            tb.Width = int.Parse(req[4]);
                            tb.Height = int.Parse(req[5]);
                            tb.Text = JoinInBounds(req, ' ', 6, req.Length);
                            tb.TextChanged += new EventHandler(OnTBTextChanged);
                            Controls.Add(tb);
                            ids.Add(tb, req[1]);
                        });
                    }
                    else if (req[0] == "add_label")
                    {
                        this.Invoke((MethodInvoker)delegate {
                            Label lbl = new Label();
                            views.Add(req[1], lbl);
                            lbl.Location = new System.Drawing.Point(int.Parse(req[2]), int.Parse(req[3]));
                            lbl.Width = int.Parse(req[4]);
                            lbl.Height = int.Parse(req[5]);
                            lbl.Text = JoinInBounds(req, ' ', 6, req.Length);
                            Controls.Add(lbl);
                            ids.Add(lbl, req[1]);
                        });
                    }
                    else if (req[0] == "add_checkbox")
                    {
                        this.Invoke((MethodInvoker)delegate {
                            CheckBox chk = new CheckBox();
                            views.Add(req[1], chk);
                            chk.Location = new System.Drawing.Point(int.Parse(req[2]), int.Parse(req[3]));
                            chk.Width = int.Parse(req[4]);
                            chk.Height = int.Parse(req[5]);
                            chk.Text = JoinInBounds(req, ' ', 6, req.Length);
                            chk.CheckedChanged += new EventHandler(OnChkCheckedChanged);
                            Controls.Add(chk);
                            ids.Add(chk, req[1]);
                        });
                    }
                    else if (req[0] == "add_radio_button")
                    {
                        this.Invoke((MethodInvoker)delegate {
                            RadioButton chk = new RadioButton();
                            views.Add(req[1], chk);
                            chk.Location = new System.Drawing.Point(int.Parse(req[2]), int.Parse(req[3]));
                            chk.Width = int.Parse(req[4]);
                            chk.Height = int.Parse(req[5]);
                            chk.Text = JoinInBounds(req, ' ', 6, req.Length);
                            chk.CheckedChanged += new EventHandler(OnRadioCheckedChanged);
                            Controls.Add(chk);
                            ids.Add(chk, req[1]);
                        });
                    }
                    else if (req[0] == "dialog")
                    {
                        this.Invoke((MethodInvoker)delegate {
                            client.SendString(MessageBox.Show(req[1].Replace('\f', ' '), req[2].Replace('\f', ' '),
                                (MessageBoxButtons)Enum.Parse(typeof(MessageBoxButtons), req[3]),
                                (MessageBoxIcon)Enum.Parse(typeof(MessageBoxIcon), req[4])).ToString());
                        });
                    }
                    else if (req[0] == "get_view_text")
                    {
                        client.SendString("*" + views[req[1]].Text);
                    }
                    else if (req[0] == "set_view_text")
                    {
                        this.Invoke((MethodInvoker)delegate { this.views[req[1]].Text = JoinInBounds(req, ' ', 2, req.Length); });
                    }
                    else if (req[0] == "set_view_width")
                    {
                        this.Invoke((MethodInvoker)delegate { this.views[req[1]].Width = int.Parse(req[2]); });
                    }
                    else if (req[0] == "get_view_width")
                    {
                        client.SendString(this.views[req[1]].Width.ToString());
                    }
                    else if (req[0] == "set_view_height")
                    {
                        this.Invoke((MethodInvoker)delegate { this.views[req[1]].Height = int.Parse(req[2]); });
                    }
                    else if (req[0] == "get_view_height")
                    {
                        client.SendString(this.views[req[1]].Height.ToString());
                    }
                    else if (req[0] == "set_view_x")
                    {
                        this.Invoke((MethodInvoker)delegate {
                            this.views[req[1]].Location = new Point(int.Parse(req[2]), this.views[req[1]].Location.Y);
                        });
                    }
                    else if (req[0] == "get_view_x")
                    {
                        client.SendString(this.views[req[1]].Location.X.ToString());
                    }
                    else if (req[0] == "set_view_y")
                    {
                        this.Invoke((MethodInvoker)delegate {
                            this.views[req[1]].Location = new Point(this.views[req[1]].Location.X, int.Parse(req[2]));
                        });
                    }
                    else if (req[0] == "get_view_y")
                    {
                        client.SendString(this.views[req[1]].Location.Y.ToString());
                    }
                    else if (req[0] == "is_visible")
                    {
                        client.SendString(this.views[req[1]].Visible ? "True" : "False");
                    }
                    else if (req[0] == "set_visible")
                    {
                        this.Invoke((MethodInvoker)delegate { this.views[req[1]].Visible = bool.Parse(req[2]); });
                    }
                    else if (req[0] == "is_checked")
                    {
                        var view = this.views[req[1]];
                        if (view.GetType() == typeof(CheckBox))
                        {
                            client.SendString(((CheckBox)view).Checked ? "True" : "False");
                        }
                        else if (view.GetType() == typeof(RadioButton))
                        {
                            client.SendString(((RadioButton)view).Checked ? "True" : "False");
                        }
                    }
                    else if (req[0] == "set_checked")
                    {
                        this.Invoke((MethodInvoker)delegate
                        {
                            var view = this.views[req[1]];
                            if (view.GetType() == typeof(CheckBox))
                            {
                                ((CheckBox)view).Checked = bool.Parse(req[2]);
                            }
                            else if (view.GetType() == typeof(RadioButton))
                            {
                                ((RadioButton)view).Checked = bool.Parse(req[2]);
                            }
                        });
                    }
                    req = client.ReceiveString().Split(' ');
                }
            }
            catch (Exception e)
            {
                Application.Exit();
            }
        }
    }
}
