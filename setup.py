from distutils.core import setup

setup(name='pyforms',
      version='1.0-beta',
      description='A python 2.7 module to interface with Windows Forms.',
      author='ExhoAR22',
      url='https://github.com/ExhoAR22/pyforms',
      data_files=[('lib/site-packages/pyforms', ['pyforms/Backend.exe'])],
      packages=['pyforms']
     )