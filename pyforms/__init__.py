from socket import socket
from os import system
from os.path import realpath, dirname
from random import randint
from threading import Thread
from sys import maxint

# Define constants
EVENT_WINDOW_CLOSED = 'window_closed'
EVENT_WINDOW_MINIMIZED = 'window_minimized'
EVENT_WINDOW_RESTORED = 'window_restored'
EVENT_BUTTON_CLICKED = 'button_clicked'
EVENT_TEXTBOX_TEXT_CHANGED = 'textbox_text_changed'
EVENT_CHECKABLE_CHECKED_CHANGED = 'checkable_checked_changed'
DIALOG_ICON_INFO = 'Information'
DIALOG_ICON_ERROR = 'Error'
DIALOG_ICON_QUESTION = 'Question'
DIALOG_ICON_NONE = 'None'
DIALOG_ICON_WARNING = 'Warning'
DIALOG_BUTTONS_OK = 'OK'
DIALOG_BUTTONS_OK_CANCEL = 'OKCancel'
DIALOG_BUTTONS_RETRY_CANCEL = 'RetryCancel'
DIALOG_BUTTONS_YES_NO = 'YesNo'
DIALOG_BUTTONS_YES_NO_CANCEL = 'YesNoCancel'
DIALOG_BUTTONS_ABORT_RETRY_IGNORE = 'AbortRetryIgnore'
DIALOG_RESULT_ABORT = 'Abort'
DIALOG_RESULT_CANCEL = 'Cancel'
DIALOG_RESULT_IGNORE = 'Ignore'
DIALOG_RESULT_NO = 'No'
DIALOG_RESULT_OK = 'OK'
DIALOG_RESULT_RETRY = 'Retry'
DIALOG_RESULT_YES = 'Yes'

# This class represents a View, such as a button, a textbox, or a label.
# It can also be used to represent a checkbox or a radio button, but these
# have extended functionality and have their own subclass (CheckableView).
class View(object):
    _id = None
    _client = None

    def __init__(self, id, socket):
        self._id = id
        self._client = socket

    @property
    def text(self):
        self._client.send('get_view_text ' + str(self._id) + '\n')
        return self._client.recv(2048)[1:]

    @text.setter
    def text(self, text):
        self._client.send('set_view_text ' + str(self._id) + ' ' + text.replace('\n', '') + '\n')

    @property
    def width(self):
        self._client.send('get_view_width ' + str(self._id) + '\n')
        return int(self._client.recv(2048))

    @width.setter
    def width(self, new_width):
        self._client.send('set_view_width ' + str(self._id) + ' ' + str(new_width) + '\n')

    @property
    def height(self):
        self._client.send('get_view_height ' + str(self._id) + '\n')
        return int(self._client.recv(2048))

    @height.setter
    def height(self, new_height):
        self._client.send('set_view_height ' + str(self._id) + ' ' + str(new_height) + '\n')

    @property
    def is_visible(self):
        self._client.send('is_visible ' + str(self._id) + '\n')
        return self._client.recv(2048) == 'True'

    @is_visible.setter
    def is_visible(self, value):
        self._client.send('set_visible ' + str(self._id) + ' ' + str(value).lower() + '\n')

    @property
    def x(self):
        self._client.send('get_view_x ' + str(self._id) + '\n')
        return int(self._client.recv(2048))

    @x.setter
    def x(self, new_x):
        self._client.send('set_view_x ' + str(self._id) + ' ' + str(new_x) + '\n')

    @property
    def y(self):
        self._client.send('get_view_y ' + str(self._id) + '\n')
        return int(self._client.recv(2048))

    @y.setter
    def y(self, new_y):
        self._client.send('set_view_y ' + str(self._id) + ' ' + str(new_y) + '\n')

# This class represents Views that can be checked, such as a checkbox or a radio button.
class CheckableView(View):
    def __init__(self, id, socket):
        View.__init__(self, id, socket)

    @property
    def is_checked(self):
        self._client.send('is_checked ' + str(self._id) + '\n')
        return self._client.recv(2048) == 'True'

    @is_checked.setter
    def is_checked(self, value):
        self._client.send('set_checked ' + str(self._id) + ' ' + str(value).lower() + '\n')

# This class represents a Window on which Views appear.
class Window(object):
    __server = None
    __client = None
    __worker = None
    __worker_client = None
    __events = {}
    __views = {}

    def __worker_proc(self):
        msg = self.__worker_client.recv(1024)
        while True:
            if msg in self.__events:
                self.__events[msg]()
            elif EVENT_BUTTON_CLICKED in msg and EVENT_BUTTON_CLICKED in self.__events:
                self.__events[EVENT_BUTTON_CLICKED](self.__views[int(msg[len(EVENT_BUTTON_CLICKED) + 1:])])
            elif EVENT_TEXTBOX_TEXT_CHANGED in msg and EVENT_TEXTBOX_TEXT_CHANGED in self.__events:
                self.__events[EVENT_TEXTBOX_TEXT_CHANGED](self.__views[int(msg[len(EVENT_TEXTBOX_TEXT_CHANGED) + 1:])])
            elif EVENT_CHECKABLE_CHECKED_CHANGED in msg and EVENT_CHECKABLE_CHECKED_CHANGED in self.__events:
                self.__events[EVENT_CHECKABLE_CHECKED_CHANGED](self.__views[int(msg[len(EVENT_CHECKABLE_CHECKED_CHANGED) + 1:])])
            elif 'clos' in msg:
                break
            msg = self.__worker_client.recv(1024)
        self.__worker_client.close()
        self.__client.close()
        self.__server.close()

    def __init__(self, width = 850, height = 550, text = 'Window'):
        port = randint(1024, 65536)
        self.__server = socket()
        self.__server.bind(('127.0.0.1', port))
        self.__server.listen(2)
        system('start ' + dirname(realpath(__file__)) + '\\backend ' + str(port) + ' ' + str(width) + ' ' + str(height) + ' "' + text + '"')
        self.__client = self.__server.accept()[0]
        self.__worker_client = self.__server.accept()[0]
        self.__worker = Thread(target = self.__worker_proc)
        self.__worker.start()

    def close(self):
        self.__client.send('close\n')
        self.__client.close()
        self.__worker_client.close()
        self.__server.close()

    @property
    def text(self):
        self.__client.send('get_text\n')
        return self.__client.recv(2048)[1:]

    @text.setter
    def text(self, text):
        self.__client.send('set_text ' + text.replace('\n', '') + '\n')

    @property
    def width(self):
        self.__client.send('get_width\n')
        return int(self.__client.recv(2048))

    @width.setter
    def width(self, new_width):
        self.__client.send('set_width ' + str(new_width) + '\n')

    @property
    def height(self):
        self.__client.send('get_height\n')
        return int(self.__client.recv(2048))

    @height.setter
    def height(self, new_height):
        self.__client.send('set_height ' + str(new_height) + '\n')

    @property
    def is_minimized(self):
        self.__client.send('is_minimized\n')
        return bool(self.__client.recv(2048))

    @is_minimized.setter
    def is_minimized(self, new_minimized):
        self.__client.send('set_minimized ' + str(new_minimized).lower() + '\n')

    def register_event(self, event_id, event_routine):
        self.__events[event_id] = event_routine

    def unregister_event(self, event_id):
        del self.__events[event_id]

    def show_dialog(self, text, title = 'Dialog', buttons = DIALOG_BUTTONS_OK, icon = DIALOG_ICON_INFO):
        self.__client.send('dialog ' + text.replace(' ', '\f') + ' ' + title.replace(' ', '\f') + ' ' + buttons + ' ' + icon + '\n')
        return self.__client.recv(1024)

    def __next_id(self):
        id = randint(0, maxint)
        while id in self.__views:
            id = randint(0, maxint)
        return id

    def add_button(self, text = 'Button', x = 0, y = 0, width = 75, height = 23):
        id = self.__next_id()
        self.__client.send('add_button ' + str(id) + ' ' + str(x) + ' ' + str(y) + ' ' + str(width) + ' ' + str(height) + ' ' + text + '\n')
        result = View(id, self.__client)
        self.__views[id] = result
        return result

    def add_textbox(self, text = '', x = 0, y = 0, width = 75, height = 23):
        id = self.__next_id()
        self.__client.send('add_textbox ' + str(id) + ' ' + str(x) + ' ' + str(y) + ' ' + str(width) + ' ' + str(height) + ' ' + text + '\n')
        result = View(id, self.__client)
        self.__views[id] = result
        return result

    def add_label(self, text = 'Label', x = 0, y = 0, width = 75, height = 23):
        id = self.__next_id()
        self.__client.send('add_label ' + str(id) + ' ' + str(x) + ' ' + str(y) + ' ' + str(width) + ' ' + str(height) + ' ' + text + '\n')
        result = View(id, self.__client)
        self.__views[id] = result
        return result

    def add_checkbox(self, text = 'Checkbox', x = 0, y = 0, width = 75, height = 23):
        id = self.__next_id()
        self.__client.send('add_checkbox ' + str(id) + ' ' + str(x) + ' ' + str(y) + ' ' + str(width) + ' ' + str(height) + ' ' + text + '\n')
        result = CheckableView(id, self.__client)
        self.__views[id] = result
        return result

    def add_radio_button(self, text = 'Radio', x = 0, y = 0, width = 75, height = 23):
        id = self.__next_id()
        self.__client.send('add_radio_button ' + str(id) + ' ' + str(x) + ' ' + str(y) + ' ' + str(width) + ' ' + str(height) + ' ' + text + '\n')
        result = CheckableView(id, self.__client)
        self.__views[id] = result
        return result
